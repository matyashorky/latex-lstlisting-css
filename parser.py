#!/usr/bin/env python3
import csv

# Load all data
properties = []
with open("all-properties.en.tab", "r") as handle:
    data = csv.DictReader(handle, delimiter="\t")
    for row in data:
        # Do not include special `--*`
        if row["property"] == "--*":
            continue
        if row["property"] not in properties:
            properties.append(row["property"])

# Write
with open("lstlisting-css.tex", "w") as handle:
    handle.write("% CSS syntax highlighting for lstlisting\n")
    handle.write("% See https://gitlab.com/matyashorky/latex-lstlisting-css\n")
    handle.write("\n")
    handle.write("\\lstdefinelanguage{css}{\n")
    handle.write("\talsoletter={\\-},\n")
    handle.write("\tmorekeywords={\n")
    for i in range(0, len(properties), 4):
        handle.write("\t" + ",".join(properties[i : i + 4]) + ",\n")
    handle.write("\t},\n")
    handle.write("\tmorestring=[s]{:}{;},\n")
    handle.write("\tsensitive,\n")
    handle.write("\tmorecomment=[s]{/*}{*/},\n")
    handle.write("}\n")

# Done
print("Written " + str(len(properties)) + " CSS properties.")
